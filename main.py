STRING_TO_CHECK = input("Entrez une phrase : ").lower()
CHAR_TO_COMPARE = input("Entrez un caractère : ").lower()
ORDINALS = ["première",
            "deuxième",
            "troisième",
            "quatrième",
            "cinquième",
            "sixième",
            "septième",
            "huitième",
            "neuvième",
            "dixième"]


def checkplace(sentence):
    """"
    :param: Un mot ou une phrase et un caractère entrés par l'utilisateur
    :return: none
    """
    charlist = []
    ispresent = False
    if len(sentence) > 0:
        for chartmp in sentence:
            charlist.append(chartmp)
        for index, value in enumerate(charlist):
            if value == CHAR_TO_COMPARE:
                getordinal(int(index))
                # print(index)
                ispresent = True
        if not ispresent:
            print('Le caractère a n\'est pas présent dans la phrase.')


def getordinal(index):
    """"
    :param: Un entier strictement positif
    :return: none
    """
    try:
        print('On retrouve le caractère "' + CHAR_TO_COMPARE + '" à la ' + str(ORDINALS[index]) + ' position.')
    except TypeError:
        print('TypeError: L\'index spécifié n\'est pas un nombre !')
    if index <= 0 or type(index) is not int:
        raise ValueError('Le nombre doit être positif et entier !')


checkplace(STRING_TO_CHECK)
